class TasksController < ApplicationController
  before_action :authenticate_request!
  before_action :set_task, only: [:update, :index_sub]


  def index
    tasks = current_user.tasks.where(parent_id: nil)

    json_response(tasks)
  end

  def index_sub
    resp = { @task.id => @task.children }

    puts "index sub: #{resp.inspect}"

    json_response(resp)
  end

  def create
    tasks = current_user.tasks     
    task = tasks.new(title: params[:task])
    task.save

    json_response(task, :created)
  end

  def create_sub
    tasks = current_user.tasks 
    task = tasks.new(title: params[:task], parent_id: params[:parent_id])

    task.save
    resp = { task.parent_id => [ task ] }

    puts "create sub: #{resp.inspect}"

    json_response(resp)
  end

  def update
    @task.update(status: params[:status])
    head :no_content
  end

  def destroy
    tasks = current_user.tasks
    tasks.where(status: true).destroy_all

    json_response(tasks.where(parent_id: nil))
  end


  private


  def set_task
    @task = Task.find(params[:task])
  end

end
