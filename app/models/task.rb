class Task < ApplicationRecord
  belongs_to :user
  belongs_to :parent, class_name: "Task", required: false
  has_many :children, class_name: "Task", foreign_key: "parent_id", dependent: :destroy
end
