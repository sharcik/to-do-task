Rails.application.routes.draw do

  post 'auth_user' => 'authentication#authenticate_user'
  post 'create_task' => 'tasks#create'
  post 'create_sub_task' => 'tasks#create_sub'
  get  'home' => 'home#index'
  post 'get_tasks' => 'tasks#index'
  post 'get_sub_tasks' => 'tasks#index_sub'
  post 'change_status' => 'tasks#update'
  post 'delete_tasks' => 'tasks#destroy'
  post 'user_data' => 'authentication#user_data'
   
   

end


