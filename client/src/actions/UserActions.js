import { hashHistory } from 'react-router'
import axios from 'axios';
import { AUTH_USER,  
         UNAUTH_USER,
         DATA_USER,
         API_URL,
         CLEAR_USER } from '../constants/main';


export function updateUser(user){
    return {
        type: DATA_USER,
        payload: user
    }
}

export function authUser(){
    return {
        type: AUTH_USER
    }
}

export function unAuthUser(){
    return {
        type: UNAUTH_USER
    }
}

export function resetUser(){
    return {
        type: CLEAR_USER
    }
}



export function registrUser(username,password,confirm){
  return dispatch => {
    axios.post(API_URL + '/auth_user', { username,password,confirm })
    .then(response => {
      if (response.data.user){
        localStorage.token = response.data.auth_token;
        dispatch(authUser());
        dispatch(updateUser(response.data.user));
        hashHistory.push('/');
      }
    })
    .catch(function(error) {
      console.log(error);
    });
  }
}


export function loginUser(username,password){
  return dispatch => {
    axios({
      method: 'POST',
      url: API_URL + '/auth_user',
      headers: {'Authorization': 'Authorization'},
      data: { username, password }
    })
    .then(response => {
      if (response.data.auth_token){
        localStorage.token = response.data.auth_token;
        dispatch(authUser());
        dispatch(updateUser(response.data.user));
        hashHistory.push('/');
      } else {
        dispatch(unAuthUser());
      }
    })
    .catch(function(error) {
      console.log(error);
    });
  }
}

export function logoutUser() {  
  return dispatch => {
    localStorage.removeItem('token');
    dispatch(unAuthUser());
    dispatch(resetUser());
    hashHistory.push('/login');
  }
}


export function userInfo() {  
  return dispatch => {
    axios({
      method: 'post',
      url: API_URL + '/user_data',
      headers: {'Authorization': 'Bearer ' + localStorage.token }
    })
    .then(response => {
      dispatch(updateUser(response.data.user));
    })
    .catch(function(error) {
      console.log(error);
    });
  }
}