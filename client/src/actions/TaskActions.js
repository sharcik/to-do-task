import axios from 'axios';
import { API_URL,
         GET_TASKS,
         SET_PARENT_TASK,
         ADD_SUB_TASKS,
         ADD_TASK } from '../constants/main';


export function allTasks(tasks){
  return { 
    type: GET_TASKS, 
    payload: tasks 
  }
}

export function addTask(task){
  return { 
    type: ADD_TASK, 
    payload: task 
  }
}

export function addSubTasks(data){
  return {
    type: ADD_SUB_TASKS,
    payload: data
  }
}


export function setParentTask(task) {
  return {
    type: SET_PARENT_TASK,
    payload: task
  };
}







export function getSubTaski(task){
  return dispatch => {
    axios({
      method: 'post',
      url: API_URL + '/get_sub_tasks',
      headers: {'Authorization': 'Bearer ' + localStorage.token },
      data: { task }
    })
    .then(response => {
      dispatch(addSubTasks(response.data));
    })
    .catch(function(error) {
      console.log(error);
    });
  }
}


export function getTaski(){
  return dispatch => {
    axios({
      method: 'post',
      url: API_URL + '/get_tasks',
      headers: {'Authorization': 'Bearer ' + localStorage.token }
    })
    .then(response => {
      dispatch(allTasks(response.data));
    })
    .catch(function(error) {
      console.log(error);
    });
  }
}

export function createTask(task){
  return dispatch => {
    axios({
      method: 'post',
      url: API_URL + '/create_task',
      headers: {'Authorization': 'Bearer ' + localStorage.token },
      data: { task }
    })
    .then(response => {
      dispatch(addTask(response.data));
    })
    .catch(function(error) {
      console.log(error);
    });
  }
}



export function createSubTask(task,parent_id = ''){
  return dispatch => {
    axios({
      method: 'post',
      url: API_URL + '/create_sub_task',
      headers: {'Authorization': 'Bearer ' + localStorage.token },
      data: { task, parent_id }
    })
    .then(response => {
      dispatch(addSubTasks(response.data));
    })
    .catch(function(error) {
      console.log(error);
    });
  }
}

export function changeStatus(task,status){
  axios({
    method: 'post',
    url: API_URL + '/change_status',
    headers: {'Authorization': 'Bearer ' + localStorage.token },
    data: { task,status }
  })
  .catch(function(error) {
    console.log(error);
  });
}

export function clearTasks(){
  return dispatch => {
    axios({
      method: 'post',
      url: API_URL + '/delete_tasks',
      headers: {'Authorization': 'Bearer ' + localStorage.token }
    })
    .then(response => {
      dispatch(allTasks(response.data));
    })
    .catch(function(error) {
      console.log(error);
    });
  }
}
