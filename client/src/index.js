import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import App from './containers/App'
import Autor from './containers/Autor'
import Login from './containers/Login'
import Registration from './containers/Registration'
import Wrapper from './components/wrapper'
import './styles/app.scss'
import configureStore from './store/configureStore'



import { Router, Route, hashHistory, IndexRoute } from 'react-router'

function checkUser() {
  if ( !localStorage.token ){
    hashHistory.push('/login');
  }
}


const store = configureStore()

render(
  <Provider store={store}>
    <Router history={hashHistory}>
      <Route path="/" component={Wrapper} >
        <IndexRoute component={App} onEnter={checkUser} />
        <Route path="/login" component={Autor}>
          <IndexRoute component={Login}/>
          <Route path="/registration" component={Registration}/>
        </Route>
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root')
)
