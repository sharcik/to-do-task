import { GET_TASKS, 
         SET_PARENT_TASK,
         ADD_SUB_TASKS,
         ADD_TASK } from '../constants/main';

const initialState = {
  tasks: [],
  parentTask: [],
  sub_tasks: {}
}


export default function task(state = initialState,action){

  switch (action.type) {
    case GET_TASKS:
      return { ...state, tasks: action.payload, parentTask: [] };
    case SET_PARENT_TASK:
      return { ...state, parentTask: action.payload };
    case ADD_TASK:
      return { ...state, tasks: [...state.tasks, action.payload], parentTask: []  };
    case ADD_SUB_TASKS:
      return { ...state, sub_tasks: {...state.sub_tasks, ...action.payload}, parentTask: []  };


    default:
      return state
  }

}


