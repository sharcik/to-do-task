import { AUTH_USER,  
         UNAUTH_USER,
         DATA_USER,
         CLEAR_USER } from '../constants/main';

const initialState = {
	user_id: '',
  email: '',
  authenticated: false
}

export default function user(state = initialState,action){
	switch (action.type) {
    case AUTH_USER:
      return { ...state, authenticated: true };
    case UNAUTH_USER:
      return { ...state, authenticated: false };
    case DATA_USER:
      return { ...state, user_id: action.payload.id, email: action.payload.email };
    case CLEAR_USER:
      return { ...state, user_id: '', email: '' };

    default:
      return state
  }
}