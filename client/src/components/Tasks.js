import React from 'react'
import Task from './Task'

export default React.createClass({
  getInitialState: function() {     
    return { 
             sub_tasks: []
           }   
  },
  componentDidMount: function() {
    this.props.taskActions.getTaski();
  },
  componentWillReceiveProps: function(data) { 
    this.setState({ sub_tasks: data.sub_tasks });   
  },
  render() {
    var tasks = [];
    var changeStatus = this.props.taskActions.changeStatus,
        setParentTask   = this.props.taskActions.setParentTask,
        getSubTaski = this.props.taskActions.getSubTaski;
    this.props.tasks.forEach(function(task) {
      tasks.push(<Task key={'task' + task.id} task = { task }
                                              sub_tasks = { this.state.sub_tasks } 
                                              setParentTask = { setParentTask }
                                              changeStatus  = { changeStatus }
                                              getSubTaski   = { getSubTaski } />);
    }.bind(this));
    return (
      <div>
        <ul>
          {tasks}
        </ul>
      </div>
    )
  }
})















