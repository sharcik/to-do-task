import React from 'react'
import Profile from '../containers/Profile'

export default React.createClass({
  render() {
    return (
      <div>
        <div className='col-md-3'>
          <Profile />
        </div>
        <div className='col-md-9'>
          {this.props.children}
        </div>
      </div>
    )
  }
})