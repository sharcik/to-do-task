import React from 'react'


export default React.createClass({
  getInitialState: function() {     
    return { task : '',
             valid: false };   
  },
  buttonFormClick: function() {
    this.setState({ task: '' });
    var task = document.getElementById('task-form').value;
    if (this.props.parentTask.id){
      this.props.taskActions.createSubTask(task,this.props.parentTask.id);
    } else {
      this.props.taskActions.createTask(task);
    }    
  },
  validTask: function(value) {
    value ? this.setState({ valid: true }) : this.setState({ valid: false });
  },
  inputTask: function(e) {
    this.setState({ task: e.target.value });
    this.validTask(e.target.value);
  },
  render() {
    var button;

    if (this.state.valid){
      button = ( 
        <button name='task-button' onClick= { this.buttonFormClick }>Create Task</button> 
      )
    } else {
      button = ( 
        <button disabled>Create Task</button> 
      )
    }
    return (
        <div>
          <div className='create_input'>
            <input id="task-form" value={this.state.task} onChange={this.inputTask} name="task" placeholder={this.props.parentTask.title}  type="text" />
            {button}
          </div>
        </div>
    )
  }
})






