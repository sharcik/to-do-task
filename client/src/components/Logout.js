import React from 'react'


export default React.createClass({
  componentDidMount: function() {
    this.props.userActions.userInfo();
  },
  render() {
    var outLink;
    if (localStorage.token) { 
      outLink = (<p className='cursor' onClick= { (::this.props.userActions.logoutUser) }>Sign Out</p>);
    } else { 
      outLink = '';
    }
    return (
      <div className='container'>
        <h4>{this.props.user.email}</h4>
        {outLink}
      </div>
    )
  }
})