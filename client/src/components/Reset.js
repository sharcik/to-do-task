import React from 'react'


export default React.createClass({
  buttonResetClick: function() {
    this.props.taskActions.clearTasks();
  },
  render() {
    return (
      <div>
      <button onClick= { this.buttonResetClick }>Clear complited tasks</button>
      </div>
    )
  }
})