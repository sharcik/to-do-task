import React from 'react';
import Task from './Task'


export default React.createClass({
  getInitialState: function() {     
    return { 
             checked: this.props.task.status
           }   
  },
  componentWillReceiveProps: function() {
    this.setState({ checked: this.props.task.status });   
  },
  componentDidMount: function() {
    this.props.getSubTaski(this.props.task.id);     
  },
  callStatus: function() { 
    var checkbox = document.getElementById('task' + this.props.task.id),
        status;
    checkbox.checked ? status = true : status = false; 
    this.setState({ checked: status });      
    this.props.changeStatus(this.props.task.id,status);  
  },
  sendSubTask: function() { 
    document.getElementById('task-form').focus();
    this.props.setParentTask(this.props.task); 
  },
  render() {
    var subtasks = [],
        props_subtatsks = this.props.sub_tasks[this.props.task.id];
    if (props_subtatsks){
      props_subtatsks.forEach(function(sub_task) {
        subtasks.push(<Task key={'task' + sub_task.id} task = { sub_task }
                                                sub_tasks = { this.props.sub_tasks } 
                                                setParentTask = { this.props.setParentTask }
                                                changeStatus  = { this.props.changeStatus }
                                                getSubTaski   = { this.props.getSubTaski } />);
      }.bind(this));
    }
    return (
      <li className={this.state.checked ? 'completed' : ''}>
        <span>
          <span className='add_subtask' onClick = { this.sendSubTask }>Add subtask</span>
          <label>
          {this.props.task.title}       
            <input id={'task' + this.props.task.id } type="checkbox" onChange={this.callStatus} checked={this.state.checked} />
            <i></i>
          </label>
        </span>
        <div className="container-fluid sub_task_margin">
          {subtasks}
        </div>
      </li>
    )
  }
})