import React from 'react'

export default React.createClass({
  getInitialState: function() {     
    return { username : '',
             password : '',
             password_confirm : '',
             stateUsername: false, 
             statePassword: false,
             stateConfirm: false };   
  },
  buttonRegistrClick: function() {
    var password = document.getElementById('reg_password').value,
        username = document.getElementById('reg_username').value,
        confirm = document.getElementById('reg_confirm').value;
    this.props.userActions.registrUser(username,password,confirm);
  },
  validUsername: function(e) {
    e.target.value ? this.setState({ stateUsername: true }) : this.setState({ stateUsername: false });
  },
  validPassword: function(e) {
    e.target.value.length >= 8 ? this.setState({ statePassword: true }) : this.setState({ statePassword: false });
    this.validConfirm();
  },
  validConfirm: function() {
    var password = document.getElementById('reg_password').value,
        confirm = document.getElementById('reg_confirm').value;
    confirm == password ? this.setState({ stateConfirm: true }) : this.setState({ stateConfirm: false });
  },
  render() {
    var button;

    if (this.state.stateConfirm&&this.state.statePassword&&this.state.stateUsername){
      button = ( 
        <button onClick= { (::this.buttonRegistrClick) }>Registration</button> 
      )
    } else {
      button = ( 
        <button disabled>Registration</button> 
      )
    }
    return (
      <div className='text-center cont-registr'>
          <input type="text" className={this.state.stateUsername ? '' : 'red-border'} name="username" placeholder="Email" id='reg_username' onChange={this.validUsername}/>
          <input type="text" className={this.state.statePassword ? '' : 'red-border'} name="password" placeholder="Password" id='reg_password' onChange={this.validPassword}/>
          <input type="text" className={this.state.stateConfirm ? '' : 'red-border'} name="password-confirm" placeholder="Confirm password" id='reg_confirm' onChange={this.validConfirm}/>
          <div className='container-fluid'>
            {button}
          </div>
      </div>
    )
  }
});