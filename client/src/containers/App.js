import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as taskActions from '../actions/TaskActions'

import Form from '../components/Form'
import Tasks from '../components/Tasks'
import Reset from '../components/Reset'


class App extends Component {
  render() {
    return (
      <main>
        <Form taskActions={this.props.taskActions} parentTask={this.props.parentTask} />
        <Tasks taskActions={this.props.taskActions} tasks={this.props.tasks} sub_tasks={this.props.sub_tasks} />
        <Reset taskActions={this.props.taskActions}/>
      </main>
    )
  }
}

function mapDispatchToProps(dispatch){
  return{
    taskActions: bindActionCreators(taskActions, dispatch)
  }
}


function mapStateToProps(state){
  return{
    tasks: state.task.tasks,
    parentTask: state.task.parentTask,
    sub_tasks: state.task.sub_tasks
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(App)