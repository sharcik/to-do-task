import React from 'react'





export default React.createClass({
  getInitialState: function() {     
    return { username : '',
             password : '',
             stateUsername: false, 
             statePassword: false};   
  },
  buttonLoginClick: function() {
    var password = document.getElementById('log_password').value,
        username = document.getElementById('log_username').value;
    this.props.userActions.loginUser(username,password);
  },
  validUsername: function(e) {
    e.target.value ? this.setState({ stateUsername: true }) : this.setState({ stateUsername: false });
  },
  validPassword: function(e) {
    e.target.value.length >= 8 ? this.setState({ statePassword: true }) : this.setState({ statePassword: false });
  },
  render() {
    var button;

    if (this.state.statePassword&&this.state.stateUsername){
      button = ( 
        <button onClick= { (::this.buttonLoginClick) }>Login</button> 
      )
    } else {
      button = ( 
        <button disabled>Login</button> 
      )
    }
    return (
      <div className='text-center cont-registr'>
          <input type="text" name="username" placeholder="Email" id="log_username" onChange={this.validUsername}/>
          <input type="text" name="password" placeholder="Password" id="log_password" onChange={this.validPassword}/>
          <div className='container-fluid'>
            {button} 
          </div>
      </div>
    )
  }
})
