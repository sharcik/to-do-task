import React, { Component } from 'react'
import { connect } from 'react-redux'
import Logout from '../components/Logout'
import { bindActionCreators } from 'redux'
import * as userActions from '../actions/UserActions'



class Profile extends Component {
  render() {
    return (
      <div className='container'>
        <Logout user={this.props.user.user} userActions={this.props.userActions}/>
      </div>
    )
  }
}



function mapDispatchToProps(dispatch){
  return{
    userActions: bindActionCreators(userActions, dispatch)
  }
}


function mapStateToProps(state){
  return{
    user: state
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Profile)