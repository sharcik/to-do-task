import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as userActions from '../actions/UserActions'

import { Link } from 'react-router'

class Autor extends Component {
  render() {
    return (
      <main className="autor-page">
          <div className='container-fluid'>
            <div className='col-md-3'><h3><Link to="/login">Login</Link></h3></div>
            <div className='col-md-4 col-md-offset-5'><h3><Link to="/registration">Registration</Link></h3></div>
          </div>
          {this.props.children && React.cloneElement(this.props.children, { userActions: this.props.userActions })}
      </main>
    )
  }
}

function mapDispatchToProps(dispatch){
  return{
    userActions: bindActionCreators(userActions, dispatch)
  }
}


function mapStateToProps(state){
  return{
    user: state
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Autor)