class CreateTasks < ActiveRecord::Migration[5.0]
  def change
    create_table :tasks do |t|
      t.string :title
      t.boolean :status
      t.datetime :time
      t.belongs_to :user, index: true

      t.timestamps
    end
  end
end
